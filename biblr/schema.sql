drop table if exists kjv;
create table kjv (
  id integer primary key autoincrement,
  book text not null,
  chapter int not null,
  verse int not null,
  `text` text not null
);

drop table if exists comments;
create table comments (
  id integer primary key autoincrement,
  book text not null,
  chapter int not null,
  verse int not null,
  `text` text not null
);
